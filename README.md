# Hopfield net. report

Для подготовки микро-доклада по сетям Хопфилда

## Архитектура сети
- НС состоит из N искусственных нейронов
- Каждый нейрон системы может принимать одно из двух состояний (1, -1)

> Also, из-за биполярности нейроны = спины, в д.с.
...

## 

## Ограничения сети
- Относительно небольшой объем памяти (количества распознаваемых образов), при превышении - перестает запоминать образы.  (формула)
- Достижение устойчивого состояния не гарантирует правильный ответ сети. Это происходит из-за того, что сеть может сойтись к так называемым ложным аттракторам, иногда называемым "химерой" (как правило, химеры склеены из фрагментов различных образов). 

## Useful links:
- [ Нейронная сеть Хопфилда на пальцах | habr ](https://habr.com/post/301406/)
- [8. Hopfield Network model of associative memory](https://neuronaldynamics-exercises.readthedocs.io/en/latest/exercises/hopfield-network.html)
- [Discrete Hopfield Network](neupy.com/2015/09/20/discrete_hopfield_network.html)
- [Нейронная сеть Хопфилда.](https://microtechnics.ru/nejronnaya-set-xopfilda/)
- [Нейронная сеть Хопфилда](http://ru.cybernetics.wikia.com/wiki/%D0%9D%D0%B5%D0%B9%D1%80%D0%BE%D0%BD%D0%BD%D0%B0%D1%8F_%D1%81%D0%B5%D1%82%D1%8C_%D0%A5%D0%BE%D0%BF%D1%84%D0%B8%D0%BB%D0%B4%D0%B0)
- [hopfield | gitlab](https://gitlab.com/ANN-EngEle-20181/hopfield)
- [Хопфилд, Джон | wiki](http://ru.wikipedia.nom.im/wiki/%D0%A5%D0%BE%D0%BF%D1%84%D0%B8%D0%BB%D0%B4,_%D0%94%D0%B6%D0%BE%D0%BD)
